package com.flowcloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yinpan
 * @date 2020/10/10
 */
@RestController
public class SSOController {
    @GetMapping("/hello")
    public String helloPage() {
        return "Hello SSO";
    }
}
